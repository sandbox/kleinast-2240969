<?php
/**
 * @file
 * Calameo template.
 * 
 * Available variables:
 * - $atom: Objet atom.
 */
?>
<p><strong><?php print $title; ?></strong></p>
<iframe class="calameo" src="//v.calameo.com/?bkcode=<?php print $atom->base_id; ?>" width="170" height frameborder="0" scrolling="no" allowtransparency allowfullscreen style="margin:0 auto;"></iframe>
