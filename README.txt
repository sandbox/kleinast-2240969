CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Configuration
 * Maintainers

 INTRODUCTION
------------
Scald: Calameo is a calameo provider allowing Scald module users 
to add Media Atoms of type calameo, using the Calameo API.

REQUIREMENTS
------------
This module requires the following module:
 * Scald

 CONFIGURATION
-------------
Go to admin/config/content/scald-calameo and add your calameo api 
private/public keys.

MAINTAINERS
-----------
Current maintainers:
 * Bastien Picharles - https://drupal.org/user/2301140
