<?php

/**
 * @file
 * Calameo administration form.
 */

/**
 * Return form config.
 *
 * @return array
 *   Form to display
 */
function scald_calameo_settings_form() {
  $form['scald_calameo_api_key_public'] = array(
    '#type' => 'textfield',
    '#title' => t('Public key'),
    '#default_value' => variable_get('scald_calameo_api_key_public', ''),
  );
  $form['scald_calameo_api_key_private'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#default_value' => variable_get('scald_calameo_api_key_private', ''),
  );

  return system_settings_form($form);
}
